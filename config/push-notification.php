<?php

return array(

    'IOSUser'     => array(
        'environment' =>env('IOS_USER_ENV', 'development'),
        'certificate' => app_path().'/apns/user/Instauser.pem',
        'passPhrase'  => env('IOS_PUSH_PASS', 'Appoets123$'),
        'service'     =>'apns'
    ),
    'IOSProvider' => array(
        'environment' => env('IOS_PROVIDER_ENV', 'development'),
        'certificate' => app_path().'/apns/provider/instacartDelivery.pem',
        'passPhrase'  => env('IOS_PROVIDER_PUSH_PASS', 'Appoets123$'),
        'service'     => 'apns'
    ),
    'IOSShop' => array(
        'environment' => env('IOS_SHOP_ENV', 'development'),
        'certificate' => app_path().'/apns/shop/Certificates.pem',
        'passPhrase'  => env('IOS_SHOP_PUSH_PASS', 'Appoets123$'),
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' =>env('ANDROID_ENV', 'development'),
        'apiKey'      =>'AAAAs8zVZ0g:APA91bE-V9WnfH7jB2Jf3ggZJZpilc5D7g1f5N4q_M9yZGAHPAhq_I0OYuJHB83luU_jdaaOnuLL8CsdEIF1uXLPO7qoperJzSSlFJSwIqqiprE3MJ9T59GiKv73finfVu4A7aSBKDPF',
        'service'     =>'gcm'
    )

);
